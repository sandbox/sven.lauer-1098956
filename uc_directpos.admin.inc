<?php
/**
 * @file
 * DirectPOS Integration for Ubercart: Admin callbacks.
 */

/**
 * Return the credit card payment method settings form.
 *
 * @return A FAPI array.
 * @author Sven Lauer
 */
function uc_payment_method_directpos_cc_settings() {
  $form = array();
  $form['api_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('DirectPOS API Credentials'),
    '#description' => t('Your DirectPOS API Username and Password.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer uc_directpos'),
  );
  $form['api_keys']['uc_directpos_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 30,
    '#maxlength' => 255,
    '#default_value' => variable_get('uc_directpos_username', ''),
  );
  $form['api_keys']['uc_directpos_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 30,
    '#maxlength' => 64,
    '#default_value' => variable_get('uc_directpos_password', ''),
  );
  $form['api_keys']['uc_directpos_url'] = array(
    '#type' => 'textfield',
    '#title' => t('DirectPOS API URL'),
    '#description' => t('The API URL to which requests are sent.'),
    '#size' => 80,
    '#maxlength' => 255,
    '#default_value' => variable_get('uc_directpos_url', ''),
  );

  $form['texts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Texte'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer uc_directpos'),
  );
  $form['texts']['uc_directpos_failure_retry_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text für fehlgeschlagene Zahlung (Wiederholung okay)'),
    '#description' => t('Wenn eine Zahlung definitiv nicht gebucht wurde, kann der Besteller versuchen, die Zahlung, ggf. mit einem anderen Zahlungsmittel, zu wiederholen. Dieser Text wird dem Benutzer angezeigt, um ihn davon in Kentniss zu setzen.'),
    '#cols' => 60,
    '#rows' => 10,
    '#default_value' => variable_get('uc_directpos_failure_retry_text',''),
  );
    $form['texts']['uc_directpos_failure_canceled_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text für fehlgeschlagene Zahlung (Nicht wiederholen)'),
    '#description' => t('Wenn unklar ist, ob die Zahlung durchgeführt wurde oder nicht, sollte die Zahlung als storniert betrachtet werden. Setzen Sie den Besteller mit diesem Text hiervon in Kenntnis, und bitten Sie ihn, Kontakt aufzunehmen, um die Buchung zu klären/stornieren'),
    '#cols' => 60,
    '#rows' => 10,
    '#default_value' => variable_get('uc_directpos_failure_canceled_text',''),
  );
  if (module_exists('token')) {
    $form['texts']['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      );

    $form['texts']['token_help']['help'] = array(
      '#value' => theme('token_help', 'order'),
      );
  }

  return $form;
}