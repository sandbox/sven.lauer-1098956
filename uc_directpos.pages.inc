<?php
/**
 * @file
 * DirectPOS (German Postbank, among others) Integration for Ubercart: Page and form
 * callbacks
 */
/**
 * Form callback for checkout-review form.
 *
 * @return void
 * @author Sven Lauer
 */
function uc_directpos_checkout_review_form_submit(&$form, &$form_state) {
  if ($_SESSION['do_complete']){
    unset($_SESSION['do_complete']);
    $order = uc_order_load($_SESSION['cart_order']);
    $key = variable_get('uc_directpos_password', '');
//    die(print_r($order));
    $method = $order->payment_method == 'directpos_directdebit' ? 'dd' : 'cc';
    $options['query'] = uc_directpos_build_request_params($method, $order, $key);
    $options['external'] = TRUE;
    $url = variable_get('uc_directpos_url', '');
    $form_state['redirect'] = url($url, $options);
  }

}
/**
 * Callback for notification url.
 *
 * @return void
 * @author Sven Lauer
 **/
function uc_directpos_complete() {
  $params = $_POST;
  $url = url('uc_directpos/error', array('absolute' => TRUE)) . '?code=';
 // unset($params['q']);

  // verify that noone tempered with the data
  $mac = $params['mac'];
  unset($params['mac']);


  $key = variable_get('uc_directpos_password', '');
  if (uc_directpos_create_mac($key, $params) != $mac) {
    watchdog('uc_directpos', 'Bad MAC: '.$mac."<br />\n".'Should be: '.uc_directpos_create_mac($key, $params));
        uc_order_update_status($params['basketid'], uc_order_state_default('payment_error'));

    exit('redirecturlf='. $url . 'invalid_mac&retry=no');
  }

  // Check if the payment transaction was successful
  if ($params['directPosErrorCode'] != 0) {
    watchdog('uc_directpos', 'Gateway Error for order ' . $params['orderid'] . ': Errorcode: ' . $params['directPosErrorCode'] . ' / '. $params['rc']);
    if (uc_directpos_lookup_error_code($params['directPosErrorCode'], $params['rc']) == 'retry') {
       exit('redirecturlf='. $url . 'gateway_error&retry=yes');
    } else {
    uc_order_update_status($params['basketid'], uc_order_state_default('payment_error'));
       exit('redirecturlf='. $url . 'gateway_error&retry=no');
    }


  }

  // Verify the order exists
  $order = uc_order_load($params['basketid']);
  if (!$order) {
     exit('redirecturlf='. $url . 'order_not_found&retry=yes');
  }

  // Verify that the price is right
   $context = array(
    'revision' => 'formatted',
    'type' => 'amount',
    'subject' => array(
      'order' => $order,
    ),
  );
  $options = array(
    'sign' => FALSE,
    'dec' => ',',
    'thou' => FALSE,
  );
  if ($params['amount'] != uc_price($order->order_total, $context, $options)) {
        uc_order_update_status($order->order_id, uc_order_state_default('payment_error'));
      exit('redirecturlf='. $url . 'price_not_correct&retry=no');
     }
  $options['dec'] = '.';
  $paid = uc_price($order->order_total, $context, $options);

  // If we have proceeded until here, everything went fine
  $comment = "DirectPOS Transaktionsnummer: ".check_plain($params['trefnum']);
  uc_payment_enter($order->order_id, $order->payment_method, $paid, 0, NULL, $comment);
//  uc_cart_complete_sale($order);
watchdog('uc_directpos', 'Successful payment.<br/>'.var_export($params,TRUE));
  uc_order_comment_save($order->order_id, 0, 'Zahlung von EUR' . check_plain($params['amount']) . ' via DirectPOS ausgeführt', 'admin');
  uc_cart_empty(uc_cart_get_id());
  exit('redirecturls=' . url('uc_directpos/success', array('absolute' => TRUE)));
}
function uc_directpos_error_page() {
  $order = uc_order_load($_SESSION['cart_order']);
  if ($_GET['retry'] == 'yes') {
    return token_replace(variable_get('uc_directpos_failure_retry_text', ""), 'order', $order);
  } else {
    return token_replace(variable_get('uc_directpos_failure_canceled_text', ''), 'order', $order);
  }

}
function uc_directpos_success_page() {
  $_SESSION['do_complete'] = TRUE;
  drupal_goto('/cart/checkout/complete');
}
