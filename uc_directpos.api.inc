<?php
/**
 * @file
 * Functions for talking to the DirectPOS API.
 */

 /**
 * Build an array of key-val pairs that can serve as request params.
 *
 * @param $method
 *   One of 'cc', 'dd'. Defaults to 'cc'
 * @param $order
 *   An ubercart order-object.
 * @param $key
 *   The 'password' provided by your DirectPOS provider, used as an encryption key.
 *
 * @return Array
 *   Array of key-val pairs that can serve as parameters for requests
 *   to the DirectPOS server.
 * @author Sven Lauer
 */
function uc_directpos_build_request_params($method, $order, $key) {
  $param = array();
  $context = array(
    'revision' => 'formatted',
    'type' => 'amount',
    'subject' => array(
      'order' => $order,
    ),
  );
  $options = array(
    'sign' => FALSE,
    'dec' => ',',
    'thou' => FALSE,
  );

  $param['command'] = 'sslform';
  $param['amount'] = uc_price($order->order_total, $context, $options);
  $param['currency'] = 'EUR';
  $param['orderid'] = $order->order_id;
  $param['basketid'] = $order->order_id;
  $param['payment_options'] = 'sslifvisaenrolledu';
  $param['autocapture'] = 0;
  $param['paymentmethod'] = 'creditcard';
  if ($method == 'dd') {
    $param['paymentmethod'] = 'directdebit';
  }
 // $param['sessionid'] = session_id();
  $param['sslmerchant'] = variable_get('uc_directpos_username', '');
//  $param['version'] = '1.5';
  $param['mac'] = uc_directpos_create_mac($key, $param);
  return $param;
}
/**
 * Create the MAC verification hash
 *
 * The data string passed to the hmacsha1
 * function is simply a concatenation of
 * all field values, ordered by the field names.
 *
 * @param $key
 *   The key used for hashing.
 * @param $params
 *   An array of key-value pairs that should be hashed.
 *
 * @return A string representation of $params hashed with $key.
 * @author Sven Lauer
 */
function uc_directpos_create_mac($key, $params) {
  ksort($params);
  return hmacsha1($key, implode('', $params));
}
/**
 * Calculate the hash value.
 *
 * @param $key
 *  The key to hash with.
 * @param $data
 *  An array of data to be hashed
 *
 * @return $data hmac-hashed with $key.
 */
function hmacsha1($key, $data) {
  $blocksize = 64;
  $hashfunc = 'sha1';
  if (drupal_strlen($key) > $blocksize) {
    $key = pack('H*', $hashfunc($key));
  }
  $key  = str_pad($key, $blocksize, chr(0x00));
  $ipad = str_repeat(chr(0x36), $blocksize);
  $opad = str_repeat(chr(0x5c), $blocksize);
  $hmac = pack('H*', $hashfunc(($key^$opad)
        . pack('H*', $hashfunc(($key^$ipad) . $data))));
  return bin2hex($hmac);
}

function uc_directpos_lookup_error_code($code, $rc) {
  switch ($code) {
    case '100':
    case '103':
    case '104':
      if ($rc == '899') {
        return 'cancel';
      } else {
        return 'retry';
      }
    case '133':
    case '141':
    case '157':
    case '158':
    case '159':
    case '160':
    case '162':
    case '165':
    case '166':
    case '172':
    case '197':
    case '198':
    case '300':
    case '303':
    case '304':
    case '305':
    case '307':
    case '308':
    case '309':
    case '310':
    case '312':
    case '313':
    case '314':
    case '315':
    case '316':
    case '317':
    case '318':
    case '319':
    case '320':
    case '321':
    case '322':
    case '323':
    case '324':
    case '332':
    case '333':
    case '334':
    case '333':
    case '334':
    case '335':
    case '336':
    case '337':
    case '338':
    case '339':
    case '341':
    case '342':
    case '343':
    case '344':
    case '345':
    case '346':
    case '347':
    case '349':
    case '350':
    case '351':
    case '352':
    case '353':
    case '354':
    case '355':
    case '356':
    case '357':
    case '358':
    case '360':
    case '361':
    case '362':
      return 'retry';
    case '102':
    case '106':
    case '107':
    case '108':
    case '118':
    case '151':
    case '156':
    case '186':
    case '199':
    case '359':
    default:
      return 'cancel';
  break;
}

}